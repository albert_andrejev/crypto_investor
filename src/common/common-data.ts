import Serializable from './serializable';

export default class CommonData extends Serializable {
  id: number;
  updatedAt: Date;
  createdAt: Date;
}