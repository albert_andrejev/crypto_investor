import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { File } from '@ionic-native/file';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
//import { Chart } from 'chart.js/src/chart.js';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { OperationsPage } from '../pages/operations/operations';
import { CurrenciesPage } from '../pages/currencies/currencies';
import { InvestorsPage } from '../pages/investors/investors';
import { DepositPage } from '../pages/deposit/deposit';

import { AddCurrencyModalPage } from '../pages/modals/addCurrency/AddCurrency';
import { AddOperationModalPage } from '../pages/modals/addOperation/AddOperation';
import { SellOperationModalPage } from '../pages/modals/sell-operation/sell-operation';
import { AddInvestorModalPage } from '../pages/modals/add-investor/add-investor';
import { AddDepositModalPage } from '../pages/modals/add-deposit/add-deposit';
import { BackupRestoreModalPage } from '../pages/modals/backup-restore/backup-restore';
import { WithdrawModalPage } from '../pages/modals/withdraw/withdraw';

import { SettingsSelectorPopover } from '../pages/popover/settings-selector/settings-selector';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CoinMarketCapProvider } from '../providers/coin-market-cap/coin-market-cap';
import { InvestorOperationsProvider } from '../providers/investor-operations/investor-operations';
import { AssetsOperationsProvider } from '../providers/assets-operations/assets-operations';
import { CurrencyOperationsProvider } from '../providers/currency-operations/currency-operations';
import { DepositOperationsProvider } from '../providers/deposit-operations/deposit-operations';
import { DepositStatsProvider } from '../providers/deposit-stats/deposit-stats';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CurrenciesPage,
    InvestorsPage,
    OperationsPage,
    DepositPage,
    AddCurrencyModalPage,
    AddOperationModalPage,
    SellOperationModalPage,
    AddInvestorModalPage,
    AddDepositModalPage,
    BackupRestoreModalPage,
    WithdrawModalPage,
    SettingsSelectorPopover
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    PipesModule,
    ChartsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CurrenciesPage,
    InvestorsPage,
    OperationsPage,
    DepositPage,
    AddCurrencyModalPage,
    AddOperationModalPage,
    SellOperationModalPage,
    AddInvestorModalPage,
    AddDepositModalPage,
    BackupRestoreModalPage,
    WithdrawModalPage,
    SettingsSelectorPopover
  ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CoinMarketCapProvider,
    InvestorOperationsProvider,
    AssetsOperationsProvider,
    CurrencyOperationsProvider,
    DepositOperationsProvider,
    DepositStatsProvider
  ]
})
export class AppModule {}
