export enum StorageKey {
    CURRENCIES = "currencies",
    ASSETS = "assets.",
    INVESTORS = "investors",
    DEPOSITS = "deposits."
}