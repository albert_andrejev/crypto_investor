import { Pipe, PipeTransform } from '@angular/core';
import { Const } from '../../common/const';

/**
 * Generated class for the CoinConverterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'coinConverter',
})
export class CoinConverterPipe implements PipeTransform {
  transform(value: string, ...args) {
    const totalCoins = Number(value);
    if (isNaN(totalCoins) ) {
      return NaN;
    }

    return totalCoins / Const.COIN_NORM;
  }
}
