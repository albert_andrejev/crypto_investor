import { Component } from '@angular/core';
import { NavParams, ModalController, AlertController } from 'ionic-angular';

import { AddOperationModalPage } from '../modals/addOperation/AddOperation';
import { SellOperationModalPage } from '../modals/sell-operation/sell-operation';

import { AssetsOperationsProvider } from '../../providers/assets-operations/assets-operations';
import { Const } from '../../common/const';
@Component({
  selector: 'page-operations',
  templateUrl: 'operations.html'
})
export class OperationsPage {
  currency: any;
  operations: any;
  coinNorm: number = Const.COIN_NORM;

  constructor(
    public params: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    private assetsOps: AssetsOperationsProvider
  ) 
  {
    this.currency = this.params.get('currency');
  }

  ionViewWillEnter(): boolean {
    this.loadOperations();
    return true;
  }

  private loadOperations () {
    this.assetsOps.load(this.currency).then((operations) => {
      this.operations = operations;  
    });
  }

  removeOperation(ev, operation) {
    ev.stopPropagation();
    let prompt = this.alertCtrl.create({
      title: 'Delete operation',
      message: "Are you sure want to delete this operation?",
      buttons: [
        {
          text: 'Cancel',
          handler: data => {}
        },
        {
          text: 'Delete',
          handler: data => {
            this.assetsOps.remove(operation, this.currency).then((operations) => {
              this.operations = operations;  
            });
          }
        }
      ]
    });
    prompt.present();
  }

  addOperation() {    
    var params = {
      currency: this.currency
    }
    let modal = this.modalCtrl.create(AddOperationModalPage, params);
    modal.onDidDismiss(() => {
      this.loadOperations();
    });
    modal.present();
  }

  sellOperation() {    
    var params = {
      currency: this.currency
    }
    let modal = this.modalCtrl.create(SellOperationModalPage, params);
    modal.onDidDismiss(() => {
      this.loadOperations();
    });
    modal.present();
  }
}
