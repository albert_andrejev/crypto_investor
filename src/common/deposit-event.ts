import { DepositType } from './deposit-type';
import { SystemEvent } from './system-event';

export class DepositEvent {
    public static EVENT_TYPE: string = SystemEvent.DEPOSIT;
    public static DEPOSIT: string = DepositEvent.EVENT_TYPE + DepositType.DEPOSIT;
    public static WITHDRAW: string = DepositEvent.EVENT_TYPE + DepositType.WITHDRAW;
    public static BUY_ASSET: string = DepositEvent.EVENT_TYPE + DepositType.BUY_ASSET;
    public static SELL_ASSET: string = DepositEvent.EVENT_TYPE + DepositType.SELL_ASSET;
}