import { Storage } from '@ionic/storage';
import CommonData from '../common/common-data';

export default class CommonOperations {
	constructor(protected storage: Storage) {}
	
	public getLastId(values: CommonData[]) {
		let maxNumber = 0;
		values.forEach(val => {
			if (val.id > maxNumber) {
				maxNumber = val.id;
			}
		});

		return maxNumber;
	}
}