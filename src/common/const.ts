export enum Const {
    CURRENCY_NORM = Math.pow(10, 6),
    COIN_NORM = Math.pow(10, 18)
}