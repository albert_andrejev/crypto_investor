import { Component } from '@angular/core';
import { ModalController, NavController, AlertController, PopoverController, ToastController } from 'ionic-angular';

import { InvestorData } from '../../common/investor-data';

import { DepositPage } from '../deposit/deposit';
import { AddInvestorModalPage } from '../modals/add-investor/add-investor';

import { InvestorOperationsProvider } from '../../providers/investor-operations/investor-operations';
import { DepositStatsProvider } from '../../providers/deposit-stats/deposit-stats';


@Component({
  selector: 'page-investors',
  templateUrl: 'investors.html'
})
export class InvestorsPage {
  investors: InvestorData[];
  availableDepo: number = 0;
  totalShares: number = 0;
  totalBalance: number = 0;
  totalWithdraw: number = 0;
  pieChartLabels: string[] = [];
  pieChartData: number[] = [];
  loadChart = false;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public popoverCtrl: PopoverController,
    private investorOps: InvestorOperationsProvider,
    private depoStats: DepositStatsProvider
  ) { }

  ionViewWillEnter(): boolean {
    this.loadChart = false;

    const investorsPromise = this.loadInvestors();

    const totalSharesPromise = this.depoStats.totalShares.then(amount => {
      this.totalShares = amount

      return amount;
    });

    this.depoStats.availableDepo.then(availableDepo => {
      this.availableDepo = availableDepo;
    });

    this.depoStats.totalBalance.then(balance => {
      this.totalBalance = balance
    });

    this.depoStats.totalWithdraw.then(withdraw => {
      this.totalWithdraw = withdraw
    });

    Promise.all([investorsPromise, totalSharesPromise]).then((data: [InvestorData[], number]) => {
      const investors = data[0];
      const totalShares = data[1];

      this.calcPieGraphData(investors, totalShares);
    })

    return true;
  }

  private calcPieGraphData(investors, totalShares) {    
    this.pieChartLabels.length = 0;
    this.pieChartData.length = 0;
    investors.forEach(investor => {
      this.pieChartLabels.push(investor.fullName);
      let part = investor.shares * 100 / totalShares;
      part = Math.round(part * 100) / 100;
      this.pieChartData.push(part);
    });
    this.loadChart = investors.length > 1;
  }

  private loadInvestors() {
    return this.investorOps.load().then(investors => {
      this.investors = investors;

      return investors;
    });
  }

  removeInvestor(ev, investor: InvestorData) {
    ev.stopPropagation();

    if ((investor.isMain || this.investors.length < 2) && investor.deposit > 0) {
      let toast = this.toastCtrl.create({
        message: 'Unable to remove this investor. Main or last investor with deposit can not be removed!',
        duration: 3000
      });
      toast.present();

      return;
    }

    let mainInvestor = this.investors.find(investor => {
      if (investor.isMain) return true;
      return false;
    });

    if (mainInvestor === undefined) {
      mainInvestor = this.investors[0];
    }

    let prompt = this.alertCtrl.create({
      title: 'Remove investor',
      message: "Are you sure want to remove this investor? " +
        "This will move investor's deposit to " + mainInvestor.fullName + ". " +
        "In order to confirm removal please type '<strong>" + investor.fullName + "</strong>' in the prompt below.",
      inputs: [
        {
          name: 'confirmation',
          placeholder: 'Confirmation word'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
          }
        },
        {
          text: 'Delete',
          handler: data => {
            if (data.confirmation === investor.fullName) {
              console.log('removed');
              this.loadChart = false;
              this.investorOps.remove(investor, mainInvestor).then(investors => {
                this.investors = investors;
                this.calcPieGraphData(investors, this.totalShares);
              });;
            } else {
              let toast = this.toastCtrl.create({
                message: 'Wrong confirmation word',
                duration: 3000
              });
              toast.present();
            }
          }
        }
      ]
    });
    prompt.present();
  }

  openInvestorModal(params) {
    console.log("opening modal");
    let modal = this.modalCtrl.create(AddInvestorModalPage, params);
    modal.onDidDismiss(() => {
      this.loadChart = false;
      this.loadInvestors().then(investors => {
        this.calcPieGraphData(investors, this.totalShares);
      });
    });
    modal.present();
  }

  openDeposits(investor) {
    this.navCtrl.push(
      DepositPage,
      { investor: investor }
    );

    console.log(investor);
  }

}
