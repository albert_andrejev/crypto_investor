import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import CommonOperations from '../../common/common-operations';

import { StorageKey } from '../../common/storage-keys';
import { InvestorData } from '../../common/investor-data';

import { DepositOperationsProvider } from '../deposit-operations/deposit-operations';

/*
  Generated class for the InvestorOperationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InvestorOperationsProvider extends CommonOperations {


  constructor(
    storage: Storage,
    private depoOps: DepositOperationsProvider
  ) {
    super(storage);
  }


  public load(simple = false) {
    return this.storage.get(StorageKey.INVESTORS).then((investors: InvestorData[]) => {
      if (investors === null || investors.length == 0) {
        return [];
      }

      if (simple) return investors;

      let fieldsPromises = [];

      let newInvestors = investors.map(investorJSON => {
        let investor = new InvestorData().fromJSON(investorJSON);
        if (!investor.name) {
          investor.name = investor.fullName;
        }

        if (!investor.description) {
          investor.description = '';
        }

        if (investor.email) {
          let newLine = "";
          if (investor.description.length) {
            newLine = "\n";
          }
          investor.description += newLine + "Email: " + investor.email;
        }
        if (investor.phone) {
          let newLine = "";
          if (investor.description.length) {
            newLine = "\n";
          }
          investor.description += newLine + "Phone: " + investor.phone;
        }
        let calcPromise = investor.calcFields(this.depoOps);
        fieldsPromises.push(calcPromise);

        return investor;
      });

      return Promise.all(fieldsPromises).then(() => {        
        return newInvestors;
      });
    });
  }

  public backup() {
    return this.storage.get(StorageKey.INVESTORS).then((investors: InvestorData[]) => {
      if (investors === null || investors.length == 0) {
        return [];
      }

      let depoPromises = [];

      investors.forEach(investor => {
        const depositPromise = this.depoOps.backup(investor).then(deposits => {
          investor.deposits = deposits;
          return investor;
        })  

        depoPromises.push(depositPromise);
      });

      return Promise.all(depoPromises).then((investors: InvestorData[]) => {        
        return investors;
      });
    });
  }

  public restore(investors: InvestorData[]) {
    const unsetObj = investors.map(investor => {
      const investorCopy = Object.assign({}, investor);
      delete investorCopy.deposits;

      return investorCopy;
    });

    return this.storage.set(StorageKey.INVESTORS, unsetObj).then(_ => {
      return unsetObj;
    });    
  }

  public save(investor: InvestorData) {
    return this.load().then((investors: InvestorData[]) => {
      if (investors === null) {
        investors = [];
      }

      investor.updatedAt = new Date();


      if (investor.id !== null && investor.id !== 0) {
        let pos = investors.map(function (e) { return e.id; }).indexOf(investor.id);
        if (pos >= 0) {
          investors[pos] = investor;
        }
      } else {
        investor.id = this.getLastId(investors) + 1;
        investor.isMain = (investor.id === 1);
        investor.createdAt = new Date();
        investors.push(investor);
      }

      let sorted = investors.sort(function (a: InvestorData, b: InvestorData) {
        let aFullName = a.firstName + ' ' + a.lastName;
        let bFullName = b.firstName + ' ' + b.lastName;
        if (aFullName == bFullName) return 0;
        return aFullName > bFullName ? -1 : 1;
      });

      this.storage.set(StorageKey.INVESTORS, sorted);

      let investorObj = new InvestorData().fromJSON(investor);
      investorObj.calcFields(this.depoOps);

      return investorObj;
    });
  }

  public remove(fromInvestor: InvestorData, toInvestor: InvestorData) {
    //Move deposit to another investor (main or first investor) and 
    //remove investor from investors list
    this.depoOps.move(fromInvestor, toInvestor);

    return this.load().then(investors => {
      let newInvestorsList = [];
      investors.forEach((investor) => {
        if (investor.id !== fromInvestor.id) {
          newInvestorsList.push(investor);
        }
      });

      this.storage.set(StorageKey.INVESTORS, newInvestorsList);
      return newInvestorsList;
    });
  }

  public clear() {
    return this.storage.remove(StorageKey.INVESTORS);
  }
}
