#!/bin/bash

ionic cordova build android --prod --release &&

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 \
-keystore /home/albert/disks/data/Private/sci_keys/sci_key.jks \
/opt/storage/albert/Projects/SmartCryptoInvestor/platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk sci &&

rm SmartCryptoInvestor.apk &&

zipalign -v 4 /opt/storage/albert/Projects/SmartCryptoInvestor/platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk \
SmartCryptoInvestor.apk &&

apksigner verify SmartCryptoInvestor.apk

