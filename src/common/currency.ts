import { Events } from 'ionic-angular';

import { CoinMarketCapProvider } from '../providers/coin-market-cap/coin-market-cap';
import { AssetsOperationsProvider } from '../providers/assets-operations/assets-operations';

import Serializable from './serializable';
import { CurrencyEvent } from './currency-event';
import { OperationData } from './operation-data';
import { DepositType } from './deposit-type';
import { CoinMarketCapPrice } from './coin-market-cap-price';
import { Const } from './const';

export class Currency extends Serializable {
  id: string;
  name: string;
  symbol: string;
  isFiat: boolean;
  spendTotal: number = 0;
  spendAssets: number = 0;
  soldTotal: number = 0;
  currentPrice: number = 0;
  assetPricePerUnit: number = 0;
  priceDiff: number = 0;
  operations: OperationData[];

  constructor(
    private events: Events,
    private cmcProvider: CoinMarketCapProvider,
    private assetOps: AssetsOperationsProvider
  ) {
    super();
  }

  subscribeToEvents() {
    this.events.subscribe(CurrencyEvent.EVENT_TYPE + this.id, (operation: OperationData) => {
      this.processOperation(operation);
    });

    this.events.subscribe(CurrencyEvent.REMOVE_OPERATION, (operation: OperationData) => {
      this.removeOperation(operation);
    });
  }

  updateOnlinePrices() {
    return this.cmcProvider.getCurrency(this.id).then((marketPrice: CoinMarketCapPrice[]) => {
      if (marketPrice.length > 0) {
        let cmcPrice = marketPrice[0];
        cmcPrice.price_usd *= Const.CURRENCY_NORM
        this.currentPrice = cmcPrice.price_usd;
        return cmcPrice;
      }

      return null;      
    });
  }

  setAssetPrice() {
    return this.assetOps.load(this).then(operations => {
      if (operations == null) return this;

      this.spendAssets = 0;
      this.spendTotal = 0;
      this.soldTotal = 0;
      operations.forEach((operation: OperationData) => {
        this.processOperation(operation);
      });

      this.assetPricePerUnit = this.spendAssets == 0 ? 0 : this.spendTotal / (this.spendAssets  / Const.COIN_NORM);

      return this;
    });
  }

  private processOperation(operation: OperationData) {
    if (operation.type == DepositType.SELL_ASSET) {      
      this.soldTotal += (operation.price + operation.priceChange) * (operation.amount / Const.COIN_NORM) * -1
    }
    this.spendTotal += Number(operation.amount) / Const.COIN_NORM * Number(operation.price);
    this.spendAssets += Number(operation.amount);
  }

  private removeOperation(operation: OperationData) {
    if (operation.type == DepositType.BUY_ASSET) {      
      this.spendTotal -= Number(operation.amount) / Const.COIN_NORM * Number(operation.price);
    } else if (operation.type == DepositType.SELL_ASSET) {
      this.soldTotal -= (operation.price + operation.priceChange) * operation.amount / Const.COIN_NORM * -1
    }

    this.spendAssets -= Number(operation.amount);
  }
}