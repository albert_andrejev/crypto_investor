import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';

import { Currency } from '../../common/currency';
import { InvestorData } from '../../common/investor-data';
import { DepositChange } from '../../common/deposit-data';
import { DepositEvent } from '../../common/deposit-event';

import { InvestorOperationsProvider } from '../investor-operations/investor-operations';
import { CurrencyOperationsProvider } from '../currency-operations/currency-operations';
import { Const } from '../../common/const';

@Injectable()
export class DepositStatsProvider {

  constructor(
    public events: Events,
    private investorOps: InvestorOperationsProvider,
    private currencyOps: CurrencyOperationsProvider
  ) {
    events.subscribe(DepositEvent.DEPOSIT, (changeData: DepositChange) => {
      if (isNaN(changeData.deposit) && isNaN(changeData.shares)) {
        throw new TypeError('Wrong amount variable type in "' + DepositEvent.DEPOSIT + '" event handler');
      }
      this._availableDepo += changeData.deposit;
      this._totalDepo += changeData.deposit;
      this._totalShares += changeData.shares;
    });
    events.subscribe(DepositEvent.WITHDRAW, (changeData: DepositChange) => {
      if (isNaN(changeData.deposit) && isNaN(changeData.shares)) {
        throw new TypeError('Wrong amount variable type in "' + DepositEvent.WITHDRAW + '" event handler');
      }
      this._availableDepo -= changeData.deposit;
      this._totalDepo -= changeData.deposit;
      this._totalShares += changeData.shares;
    });
    events.subscribe(DepositEvent.BUY_ASSET, (changeData: DepositChange) => {
      if (isNaN(changeData.deposit)) {
        throw new TypeError('Wrong amount variable type in "' + DepositEvent.BUY_ASSET + '" event handler');
      }
      this._availableDepo -= changeData.deposit;
      this._spendTotal += changeData.deposit;
    });
    events.subscribe(DepositEvent.SELL_ASSET, (changeData: DepositChange) => {
      if (isNaN(changeData.deposit)) {
        throw new TypeError('Wrong amount variable type in "' + DepositEvent.SELL_ASSET + '" event handler');
      }
      this._availableDepo += changeData.deposit;
      this._spendTotal -= changeData.deposit;
    });
  }

  private _spendTotal: number = null;
  get spendTotal(): Promise<number> {
    if (this._spendTotal > 0) {
      return new Promise(resolve => {
        resolve(this._spendTotal);
      });
    }
    else {
      return this.calcDepo().then(result => {
        return this._spendTotal;
      });
    }
  }

  private _availableDepo: number = null;
  get availableDepo(): Promise<number> {
    if (this._availableDepo > 0) {
      return new Promise(resolve => {
        resolve(this._availableDepo);
      });
    }
    else {
      return this.calcDepo().then(result => {
        return this._availableDepo;
      });
    }
  }

  private _totalDepo: number = 0;
  get totalDepo(): Promise<number> {
    if (this._totalDepo > 0) {
      return new Promise(resolve => {
        resolve(this._totalDepo);
      });
    }
    else {
      return this.calcDepo().then(result => {
        return this._totalDepo;
      });
    }
  }

  private _totalWithdraw: number = 0;
  get totalWithdraw(): Promise<number> {
    if (this._totalWithdraw > 0) {
      return new Promise(resolve => {
        resolve(this._totalWithdraw);
      });
    }
    else {
      return this.calcDepo().then(result => {
        return this._totalWithdraw;
      });
    }
  }

  private _totalShares: number = 0;
  get totalShares(): Promise<number> {
    if (this._totalShares > 0) {
      return new Promise(resolve => {
        resolve(this._totalShares);
      });
    }
    else {
      return this.calcDepo().then(result => {
        return this._totalShares;
      });
    }
  }

  get totalBalance(): Promise<number> {
    return this.currencyOps.load().then(currencies => {
      const avDepoPromise = this.availableDepo;
      const totalWithdrawPromise = this.totalWithdraw;
      return Promise.all([avDepoPromise, totalWithdrawPromise]).then((data: [number, number]) => {
        const availableDepo = data[0];
        const totalWithdraw = data[1];

        let totalBalance = availableDepo - totalWithdraw;

        currencies.forEach(currency => {
          totalBalance += currency.currentPrice * (currency.spendAssets /  Const.COIN_NORM);
        });

        return totalBalance;
      });
    });
  }

  public clear() {
    this._totalShares = 0;
    this._totalDepo = 0;
    this._spendTotal = 0;
    this._totalWithdraw = 0;
    this._availableDepo = 0;
  }

  public calcDepo() {
    const currencyPromise = this.currencyOps.load();
    const investorsPromise = this.investorOps.load();
    return Promise.all([currencyPromise, investorsPromise]).then((data: any[]) => {
      var currencies = data[0] as Array<Currency>;
      var investors = data[1] as Array<InvestorData>;

      this._totalShares = 0;
      this._totalDepo = 0;
      this._totalWithdraw = 0;
      investors.forEach(investor => {
        this._totalDepo += investor.deposit;
        this._totalWithdraw += investor.withdraw;
        this._totalShares += investor.shares;
      });

      this._spendTotal = 0;
      let soldTotal = 0;
      currencies.forEach(currency => {
        this._spendTotal += currency.spendTotal;
        //this._totalDepo += currency.soldTotal;
        soldTotal += currency.soldTotal;
      });

      this._availableDepo = Number((this._totalDepo + soldTotal - this._spendTotal).toFixed(2));

      return true;
    });
  }

}
