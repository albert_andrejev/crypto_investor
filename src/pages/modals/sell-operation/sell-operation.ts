import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { OperationData } from '../../../common/operation-data';
import { Currency } from '../../../common/currency';

import { AssetsOperationsProvider } from '../../../providers/assets-operations/assets-operations';
import { DepositStatsProvider } from '../../../providers/deposit-stats/deposit-stats';

import { FloatValidator } from '../../../validators/float.validator';
import { DepositValidator } from '../../../validators/deposit.validator';

@Component({
  selector: 'page-sell-operation-modal',
  templateUrl: 'sell-operation.html'
})
export class SellOperationModalPage {  
  operation: OperationData = new OperationData();
  currency: Currency;
  availableDepo: number = 0;
  private operationForm : FormGroup;  

  constructor(
    public viewCtrl: ViewController,
    public params: NavParams,
    private formBuilder: FormBuilder,
    private depoStats: DepositStatsProvider,
    private assetsOps: AssetsOperationsProvider
  ) {
    this.currency = this.params.get('currency');
     

    this.operationForm = this.formBuilder.group({
      amount: ['', Validators.compose([
        Validators.required,
        Validators.min(0),
        FloatValidator.validFloat,
        (control: FormControl) => DepositValidator.notEnoughCoins(control, this.getAvailableAssets.bind(this))
      ])],
      total: ['', Validators.compose([
        Validators.required,
        Validators.min(0),
        FloatValidator.validFloat
      ])],
      date: ['', Validators.required],
    });
  }

  public getAvailableAssets(): number {
    return this.currency.spendAssets;
  }

  ionViewWillEnter(): boolean {
    this.depoStats.availableDepo.then(availableDepo => {
      this.availableDepo = availableDepo;
    });
    
    return true;
  }

  public validation_messages = {
    'amount': [
      {type: 'required', message: 'Field is required.'},
      {type: 'validFloat', message: 'Wrong value. Should be number.'},
      {type: 'notEnoughCoins', message: 'Not enough assets to sell.'}
    ],
    'total': [
      {type: 'required', message: 'Field is required.'},
      {type: 'validFloat', message: 'Wrong value. Should be number.'}
    ],
    'date': [
      {type: 'required', message: 'Field is required.'}
    ]
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  sell() {
    this.assetsOps.sell(this.operationForm.value, this.currency).then((operations) => {
      this.viewCtrl.dismiss();  
    });
  }
}
