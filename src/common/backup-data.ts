import { Currency } from "./currency";
import { InvestorData } from "./investor-data";

export class BackupData {
  currencies: Currency[];
  investors: InvestorData[];
}