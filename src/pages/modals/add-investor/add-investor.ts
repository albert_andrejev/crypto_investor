import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { InvestorData } from '../../../common/investor-data';

import { InvestorOperationsProvider } from '../../../providers/investor-operations/investor-operations';

@Component({
  selector: 'page-add-investor-modal',
  templateUrl: 'add-investor.html'
})
export class AddInvestorModalPage {  
  investor: InvestorData = new InvestorData();
  isNew: boolean;
  private investorForm : FormGroup; 

  constructor(
    public viewCtrl: ViewController,
    private investorOps: InvestorOperationsProvider,   
    public params: NavParams,
    private formBuilder: FormBuilder
  ) {
    this.investorForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: [''],
      id: []
    });

    this.isNew = true; 
    this.investor = this.params.get('investor');
  }

  ionViewWillEnter(): boolean {
    if (this.investor != null && this.investor.name != null) {
      console.log(this.investor);
      this.investorForm.patchValue(this.investor);

      this.isNew = false;
    } 
    return true;
  }

  public validation_messages = {
    'name': [
      {type: 'required', message: 'Field is required.'}
    ]
  }

  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }

  addInvestor() {
    this.investorOps.save(this.investorForm.value).then(investor => {
      this.dismiss(investor);
    });   
  }
}
