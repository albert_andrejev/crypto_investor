import { NgModule } from '@angular/core';
import { PriceConverterPipe } from './price-converter/price-converter';
import { SharesConverterPipe } from './shares-converter/shares-converter';
import { CoinConverterPipe } from './coin-converter/coin-converter';
@NgModule({
	declarations: [PriceConverterPipe,
    SharesConverterPipe,
    CoinConverterPipe],
	imports: [],
	exports: [PriceConverterPipe,
    SharesConverterPipe,
    CoinConverterPipe]
})
export class PipesModule {}
