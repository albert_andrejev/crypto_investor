import { FormControl } from '@angular/forms';
export class FloatValidator {
  static validFloat(fc: FormControl){
    if ( fc.dirty === false && fc.touched === false ) return null;

    if ( fc.value === '') {
      return ({validFloat: true});  
    }

    let floatValue = Number(fc.value);
    if (isNaN(floatValue)) {
      return ({validFloat: true});
    }

    const floatRegexp = /^-{0,1}[0-9]*[,\.]{0,1}[0-9]*$/;
    if (floatRegexp.test(fc.value)) {
      return null;
    } else {
      return ({validFloat: true});
    }
  }
}