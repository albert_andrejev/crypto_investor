import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';

import CommonOperations from '../../common/common-operations';

import { StorageKey } from '../../common/storage-keys';

import { DepositData, DepositChange } from '../../common/deposit-data';
import { DepositType } from '../../common/deposit-type';
import { DepositEvent } from '../../common/deposit-event';
import { InvestorData } from '../../common/investor-data';
import { Const } from '../../common/const';

@Injectable()
export class DepositOperationsProvider extends CommonOperations {
  constructor(
    storage: Storage,
    public events: Events
  ) {
    super(storage);
  }

  private addToDeposit(toDeposit: DepositData[], depoToMove: DepositData[], toInvestor: InvestorData) {
    if (toDeposit === null) {
      toDeposit = new Array<DepositData>();
    }

    var fullDepo = toDeposit.concat(depoToMove);

    let sorted = fullDepo.sort((a, b) => {
      if (a.date == b.date) return 0;
      return a.date > b.date ? -1 : 1;
    });

    this.storage.set(StorageKey.DEPOSITS + toInvestor.id, sorted);
  }

  public move(fromInvestor: InvestorData, toInvestor: InvestorData) {
    //Get all deposits from old investor and move it to new investor
    let fromDepositP = this.storage.get(StorageKey.DEPOSITS + fromInvestor.id);
    let toDepositP = this.storage.get(StorageKey.DEPOSITS + toInvestor.id);

    Promise.all([fromDepositP, toDepositP]).then(deposits => {
      let fromDeposit = deposits[0] as Array<DepositData>;
      let toDeposit = deposits[1] as Array<DepositData>;

      let depoToMove = new Array<DepositData>();

      if (fromDeposit === null) {
        fromDeposit = [];
      }

      fromDeposit.forEach(depo => {
        depo.investorFK = toInvestor.id;
        depo.comment = 'Moved from ' + fromInvestor.fullName + ' due to investor removal. Old comment: \'' + depo.comment + '\'';
        depoToMove.push(depo);
      });

      this.addToDeposit(toDeposit, depoToMove, toInvestor);
      this.storage.remove(StorageKey.DEPOSITS + fromInvestor.id);
    });
  }

  load(investor: InvestorData) {
    return this.storage.get(StorageKey.DEPOSITS + investor.id).then((deposit: DepositData[]) => {
      if (deposit === null) {
        deposit = [];
      }

      return deposit;
    });
  }

  backup(investor: InvestorData) {
    return this.storage.get(StorageKey.DEPOSITS + investor.id).then((deposit: DepositData[]) => {
      if (deposit === null) {
        deposit = [];
      }

      return deposit;
    });
  }

  restore(deposits: DepositData[], investor: InvestorData) {
    return this.storage.set(StorageKey.DEPOSITS + investor.id, deposits).then(_ => {
      return deposits;
    });
  }

  deposit(deposit: DepositData, investor: InvestorData, totalShares: number, totalBalans: number) {
    deposit.deposit *= Const.CURRENCY_NORM;
    let pricePerShare = 1;
    if (totalShares > 0) {
      pricePerShare = totalBalans / totalShares;
    }

    deposit.shares = Math.trunc(deposit.deposit / pricePerShare);
    deposit.type = DepositType.DEPOSIT;

    return this.save(deposit, investor);
  }

  withdraw(withdraw: DepositData, investor: InvestorData) {
    withdraw.type = DepositType.WITHDRAW;
    withdraw.shares *= -1;
    return this.save(withdraw, investor);
  }

  save(deposit: DepositData, investor: InvestorData) {
    return this.storage.get(StorageKey.DEPOSITS + investor.id).then((deposits: DepositData[]) => {
      if (deposits === null) {
        deposits = [];
      }

      deposit.id = this.getLastId(deposits) + 1;
      deposit.investorFK = investor.id;
      deposit.currency = 'USD';
      investor.shares += deposit.shares;      

      deposits.push(deposit);

      let sorted = deposits.sort(function (a, b) {
        if (a.date == b.date) return 0;
        return a.date > b.date ? -1 : 1;
      });

      this.storage.set(StorageKey.DEPOSITS + investor.id, sorted).then(success => {
        const changeData = new DepositChange(deposit.type, deposit.deposit, deposit.shares);
        this.events.publish(DepositEvent.EVENT_TYPE + deposit.type, changeData);
      });

      return deposit;
    });
  }  

  public remove(deposit: DepositData, investor: InvestorData) {
    return this.storage.get(StorageKey.DEPOSITS + investor.id).then((deposits: DepositData[]) => {
      let newDepositsList = Array<DepositData>();
      deposits.forEach((depo) => {
        if (deposit.id !== depo.id) {
          newDepositsList.push(depo);
        }
      });

      this.storage.set(StorageKey.DEPOSITS + investor.id, newDepositsList).then(result => {
        this.removeNotification(deposit);
      });

      return newDepositsList;
    });
  }

  public removeNotification(deposit: DepositData) {
    const changeData = new DepositChange(deposit.type, deposit.deposit, deposit.shares * -1);
    this.events.publish(DepositEvent.WITHDRAW, changeData);
  }

  public clear(investor: InvestorData) {
    return this.storage.remove(StorageKey.DEPOSITS + investor.id);
  }
}
