import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ViewController, NavParams, ToastController } from 'ionic-angular';

import { InvestorData } from '../../../common/investor-data';
import { DepositData } from '../../../common/deposit-data';

import { DepositOperationsProvider } from '../../../providers/deposit-operations/deposit-operations';
import { DepositStatsProvider } from '../../../providers/deposit-stats/deposit-stats';

import { FloatValidator } from '../../../validators/float.validator';
import { DepositValidator } from '../../../validators/deposit.validator';
import { Const } from '../../../common/const';

@Component({
  selector: 'page-withdraw-modal',
  templateUrl: 'withdraw.html'
})
export class WithdrawModalPage {
  private investor: InvestorData;
  private withdrawForm: FormGroup;
  private totalBalance: number = 0;
  private totalShares: number = 0;
  private fullDepo: boolean = false;

  availableDepo: number = 0;


  constructor(
    private viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    public toastCtrl: ToastController,
    private depoOps: DepositOperationsProvider,
    private depoStats: DepositStatsProvider,
    private params: NavParams
  ) {
    this.investor = this.params.get('investor');

    this.withdrawForm = this.formBuilder.group({
      deposit: ['', Validators.compose([
        Validators.required,
        Validators.min(0),
        FloatValidator.validFloat,
        (control: FormControl) => DepositValidator.notEnough(control, this.getAvailableDepo.bind(this))
      ])],
      date: ['', Validators.required],
      comment: ['']
    });
  }

  public getAvailableDepo(): number {
    return this.availableDepo;
  }

  ionViewWillEnter(): boolean {
    const totalBalancePromise = this.depoStats.totalBalance;
    const totalSharesPromise = this.depoStats.totalShares;
    const availableDepoPromise = this.depoStats.availableDepo;

    Promise.all([totalBalancePromise, totalSharesPromise, availableDepoPromise]).then((data: any[]) => {
      this.totalBalance = data[0] as number;
      this.totalShares = data[1] as number;
      var availableDepo = data[2] as number;


      const sharesPrice = this.totalBalance / this.totalShares * this.investor.shares;
      console.log(this.totalBalance, this.totalShares, this.investor.shares, sharesPrice);
      if (sharesPrice > availableDepo) {
        this.availableDepo = availableDepo;
      } else {
        this.availableDepo = Number(sharesPrice.toFixed(2));
        this.fullDepo = true;
      }
    });

    return true;
  }


  public validation_messages = {
    'deposit': [
      { type: 'required', message: 'Field is required.' },
      { type: 'min', message: 'Amount can not be negative.' },
      { type: 'validFloat', message: 'Wrong value. Should be number.' },
      { type: 'notEnoughDepo', message: 'Not enough free deposit.' }
    ],
    'date': [
      { type: 'required', message: 'Field is required.' }
    ]
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  withdraw() {
    let withdraw = this.withdrawForm.value as DepositData;
    withdraw.deposit *= Const.CURRENCY_NORM;
    if (withdraw === null) {
      let toast = this.toastCtrl.create({
        message: 'Wrong withdraw values',
        duration: 3000
      });
      toast.present();
      return;
    }

    if (this.totalShares <= 0) {
      let toast = this.toastCtrl.create({
        message: 'Unable to withdraw. Wrong total amount of shares',
        duration: 3000
      });
      toast.present();
      return;
    }

    if (withdraw.deposit >= this.availableDepo && this.fullDepo) {
      withdraw.shares = this.investor.shares;
    } else {
      let pricePerShare = this.totalBalance / this.totalShares;
      withdraw.shares = Math.trunc(withdraw.deposit / pricePerShare);
    }

    this.depoOps.withdraw(withdraw, this.investor).then(deposit => {
      this.viewCtrl.dismiss(withdraw.deposit);
    });
  }
}
