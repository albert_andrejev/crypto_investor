import { Pipe, PipeTransform } from '@angular/core';
import { Const } from '../../common/const';

/**
 * Generated class for the PriceConverterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'priceConverter',
})
export class PriceConverterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    const price = Number(value);
    if (isNaN(price) ) {
      return NaN;
    }

    //this.currencyPipe.transform

    return price / Const.CURRENCY_NORM;
  }
}
