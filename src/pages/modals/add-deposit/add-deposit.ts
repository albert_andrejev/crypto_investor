import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ViewController, NavParams, ToastController } from 'ionic-angular';

import { InvestorData } from '../../../common/investor-data';
import { DepositData } from '../../../common/deposit-data';

import { DepositOperationsProvider } from '../../../providers/deposit-operations/deposit-operations';
import { DepositStatsProvider } from '../../../providers/deposit-stats/deposit-stats';

import { FloatValidator } from '../../../validators/float.validator';

@Component({
  selector: 'page-add-deposit-modal',
  templateUrl: 'add-deposit.html'
})
export class AddDepositModalPage {
  private depositForm: FormGroup;
  private investor: InvestorData;

  constructor(
    private viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    public toastCtrl: ToastController,
    private depoOps: DepositOperationsProvider,
    private depoStats: DepositStatsProvider,
    private params: NavParams
  ) {
    this.investor = this.params.get('investor');

    this.depositForm = this.formBuilder.group({
      deposit: ['', Validators.compose([
        Validators.required,
        Validators.min(0),
        FloatValidator.validFloat
      ])],
      date: ['', Validators.required],
      comment: ['']
    });
  }

  public validation_messages = {
    'deposit': [
      { type: 'required', message: 'Field is required.' },
      { type: 'min', message: 'Amount can not be negative.' },
      { type: 'validFloat', message: 'Wrong value. Should be number.' }
    ],
    'date': [
      { type: 'required', message: 'Field is required.' }
    ]
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  addDeposit() {
    let deposit = this.depositForm.value as DepositData;
    if (deposit === null) {
      let toast = this.toastCtrl.create({
        message: 'Wrong deposit values',
        duration: 3000
      });
      toast.present();
    }

    const totalBalancePromise = this.depoStats.totalBalance;
    const totalSharesPromise = this.depoStats.totalShares;

    Promise.all([totalBalancePromise, totalSharesPromise]).then((data: any[]) => {
      var totalBalance = data[0] as number;
      var totalShares = data[1] as number;

      this.depoOps.deposit(deposit, this.investor, totalShares, totalBalance).then(() => {
        this.viewCtrl.dismiss(deposit.deposit);
      });
    })
      .catch(error => {
        let toast = this.toastCtrl.create({
          message: error,
          duration: 3000
        });
        toast.present();
      });
  }
}
