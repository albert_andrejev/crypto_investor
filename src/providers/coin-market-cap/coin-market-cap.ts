import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Currency } from '../../common';
/*
  Generated class for the CoinMarketCapProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CoinMarketCapProvider {
  apiUrl = 'https://api.coinmarketcap.com/v1/ticker';
  private currencies: any[];

  constructor(public http: HttpClient) {
  }

  getCurrency(currencyId: string) {
    return new Promise(
      (resolve, reject) => {
        this.http.get(this.apiUrl + '/' + currencyId + '/?convert=USD').subscribe(data => {
          resolve(data);
        }, err => {
          reject(err);
        });
      }
    );
  }

  getCurrenciesList() {
    if (this.currencies != null && this.currencies.length > 0) {
      return new Promise(resolve => { resolve(this.currencies) });
    }
    return new Promise((resolve, reject) => {
      this.http.get(this.apiUrl + '/?limit=10000').subscribe((data: Currency[]) => {
        this.currencies = data.map(function (currency) {
          return {
            id: currency.id,
            name: currency.name,
            symbol: currency.symbol,
            isFiat: false
          }
        });
        resolve(this.currencies);
      }, err => {
        reject(err);
      });
    });
  }

}
