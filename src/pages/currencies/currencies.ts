import { Component } from '@angular/core';
import { ModalController, AlertController, ToastController } from 'ionic-angular';

import { AddCurrencyModalPage } from '../modals/addCurrency/AddCurrency';

import { CurrencyOperationsProvider } from '../../providers/currency-operations/currency-operations';

@Component({
  selector: 'page-currencies',
  templateUrl: 'currencies.html'
})
export class CurrenciesPage {
  currencies:any[];

  constructor(
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private currencyOps: CurrencyOperationsProvider
  ) {}

  ionViewWillEnter(): boolean {
    this.loadCurrencies();
    return true;
  }

  public loadCurrencies() {
    this.currencyOps.load().then(currencies => {
      this.currencies = currencies;
    });
  }

  removeAsset(ev, currency) {
    ev.stopPropagation();
    let prompt = this.alertCtrl.create({
      title: 'Delete currency',
      message: "Are you sure want to delete this currency? "+
      "This will delete ALL currency operations history as well. " +
      "In order to confirm deletion please type '<strong>" + currency.name + "</strong>' in the prompt below.",
      inputs: [
        {
          name: 'confirmation',
          placeholder: 'Confirmation word'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {           
          }
        },
        {
          text: 'Delete',
          handler: data => {
            if (data.confirmation === currency.name) {
              this.currencyOps.remove(currency).then(currencies => {
                this.currencies = currencies;
              });
            } else {
              let toast = this.toastCtrl.create({
                message: 'Wrong confirmation word',
                duration: 3000
              });
              toast.present();
            }
          }
        }
      ]
    });
    prompt.present();
  }

  openModal(params: any = {}) {
    console.log("opening modal");
    params.isFiat = false;
    let modal = this.modalCtrl.create(AddCurrencyModalPage, params);
    modal.onDidDismiss(() => {
      this.loadCurrencies();
    });
    modal.present();
  }

}
