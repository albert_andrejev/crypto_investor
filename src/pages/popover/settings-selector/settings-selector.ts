import { Component } from '@angular/core';
import { ModalController, ViewController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { File } from '@ionic-native/file';

import { BackupRestoreModalPage } from '../../modals/backup-restore/backup-restore';
import { BackupData } from '../../../common/backup-data';
import { InvestorOperationsProvider } from '../../../providers/investor-operations/investor-operations';
import { CurrencyOperationsProvider } from '../../../providers/currency-operations/currency-operations';
import { DepositStatsProvider } from '../../../providers/deposit-stats/deposit-stats';
import { Currency } from '../../../common/currency';
import { InvestorData } from '../../../common/investor-data';


@Component({
  selector: 'page-settings-selector',
  templateUrl: 'settings-selector.html'
})
export class SettingsSelectorPopover {

  constructor(
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public viewCtrl: ViewController,
    private investorOps: InvestorOperationsProvider,
    private currencyOps: CurrencyOperationsProvider,
    private depoStats: DepositStatsProvider,
    private file: File,
    private storage: Storage,
    public params: NavParams
  ) {}

  settings() {
    
  }

  backup() {
    const today = new Date();
    const backupFilename = 'Backup-' + today.toISOString().replace(/:/g,"-") + '.json';
    let prompt = this.alertCtrl.create({
      title: 'Backup',
      message: "New backup with name <strong>'" + backupFilename + "'</strong> will be created!",
      buttons: [
        {
          text: 'Cancel',
          handler: data => { }
        },
        {
          text: 'Create',
          handler: data => {
            const currencyPromise = this.currencyOps.backup();
            const investorPromise = this.investorOps.backup();
            Promise.all([currencyPromise, investorPromise]).then((backup: [Currency[], InvestorData[]]) => {
              const backupData = new BackupData();
              backupData.currencies = backup[0];
              backupData.investors = backup[1];

              console.log(JSON.stringify(backupData));
              this.file.createDir(this.file.dataDirectory, 'backup', true)
                .then(_ => {
                  this.file.writeFile(this.file.dataDirectory, 'backup/' + backupFilename, JSON.stringify(backupData), { replace: true })
                    .then(_ => {
                      let toast = this.toastCtrl.create({
                        message: 'Backup created sucessfully',
                        duration: 3000
                      });
                      toast.present();
                    })
                    .catch(err => {
                      console.log(JSON.stringify(err));
                      let toast = this.toastCtrl.create({
                        message: 'Backup failed. Unable to create backup. Error: ' + err.message,
                        duration: 3000
                      });
                      toast.present();
                    });
                })
                .catch(err => {
                  let toast = this.toastCtrl.create({
                    message: 'Backup failed. Unable to create backup directory. Error: ' + err,
                    duration: 3000
                  });
                  toast.present();
                });
            });
          }
        }
      ]
    });
    prompt.present();
    this.viewCtrl.dismiss();
  }

  restore() {
    let modal = this.modalCtrl.create(BackupRestoreModalPage);
    const restorePromise = new Promise((resolve, reject) => {
      modal.onDidDismiss((result) => {
        if (result) {
          resolve();
        } else {
          reject("Restore cancelled");
        }
      });
    });    
    modal.present();
    this.viewCtrl.dismiss({restore: restorePromise});
  }

  clearData(ev) {
    ev.stopPropagation();
    let prompt = this.alertCtrl.create({
      title: 'Clear ALL Data',
      message: "Are you sure want to ALL data? "+
      "Please make sure you create backup before clearing. " +
      "In order to confirm clearing please type '<strong>CLEAR</strong>' in the prompt below.",
      inputs: [
        {
          name: 'confirmation',
          placeholder: 'Confirmation word'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {      
            this.viewCtrl.dismiss();     
          }
        },
        {
          text: 'Clear ALL',
          handler: data => {
            if (data.confirmation === 'CLEAR') {
              const clearPromise = this.storage.clear().then(_ => {
                this.depoStats.clear();                
                return _;
              });
              this.viewCtrl.dismiss({restore: clearPromise});
            } else {
              let toast = this.toastCtrl.create({
                message: 'Wrong confirmation word',
                duration: 3000
              });
              toast.present();
              this.viewCtrl.dismiss();
            }
          }
        }
      ]
    });
    prompt.present();
  }
}
