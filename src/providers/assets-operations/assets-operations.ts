import { Storage } from '@ionic/storage';
import { StorageKey } from '../../common/storage-keys';
import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';

import { Currency } from '../../common/currency';
import { CurrencyEvent } from '../../common/currency-event';

import CommonOperations from '../../common/common-operations';

import { OperationData } from '../../common/operation-data';
import { DepositChange } from '../../common/deposit-data';
import { DepositEvent } from '../../common/deposit-event';
import { DepositType } from '../../common/deposit-type';
import { Const } from '../../common/const';

@Injectable()
export class AssetsOperationsProvider extends CommonOperations {

  constructor(
    public events: Events,
    public storage: Storage
  ) {
    super(storage);
  }

  public load(currency: Currency) {
    return this.storage.get(StorageKey.ASSETS + currency.id).then((operations: OperationData[]) => {
      if (operations === null) {
        return [];
      }

      return operations;
    });
  }

  public backup(currency: Currency) {
    return this.storage.get(StorageKey.ASSETS + currency.id).then((operations: OperationData[]) => {
      if (operations === null) {
        return [];
      }

      return operations;
    });
  }

  public restore(operations: OperationData[], currency: Currency) {
    return this.storage.set(StorageKey.ASSETS + currency.id, operations).then(_ => {
      return operations;
    });
  }

  public buy(operation: OperationData, currency: Currency) {
    operation.total *= Const.CURRENCY_NORM;
    operation.type = DepositType.BUY_ASSET;
    operation.price = Math.trunc(operation.total / operation.amount);
    operation.amount *= Const.COIN_NORM;

    this.events.publish(CurrencyEvent.EVENT_TYPE + currency.id, operation);

    return this.save(operation, currency);
  }

  public sell(operation: OperationData, currency: Currency) {    
    operation.total *= Const.CURRENCY_NORM;
    operation.type = DepositType.SELL_ASSET;
    operation.priceChange = Math.trunc(operation.total / operation.amount) - currency.assetPricePerUnit;
    operation.amount *= Const.COIN_NORM * -1;
    operation.price = Math.trunc(currency.assetPricePerUnit);

    this.events.publish(CurrencyEvent.EVENT_TYPE + currency.id, operation);

    return this.save(operation, currency);
  }
  
  public save(operation: OperationData, currency: Currency) {
    return this.storage.get(StorageKey.ASSETS + currency.id).then((operations: OperationData[]) => {
      if (operations === null) {
        operations = [];
      }

      operation.id = this.getLastId(operations) + 1;
      operation.currency_id = currency.id;
      operation.createdAt = new Date();
      operations.push(operation);

      let sorted = operations.sort(function (a, b) {
        if (a.date == b.date) return 0;
        return a.date > b.date ? -1 : 1;
      });

      this.storage.set(StorageKey.ASSETS + currency.id, sorted).then(result => {
        const changeData = new DepositChange(operation.type, operation.total, 0);
        this.events.publish(DepositEvent.EVENT_TYPE + operation.type, changeData);
      });

      return sorted;
    });
  }

  public remove(operation: OperationData, currency: Currency) {
    return this.storage.get(StorageKey.ASSETS + currency.id).then((operations: OperationData[]) => {
      let newOperationsList = Array<OperationData>();
      operations.forEach((op) => {
        if (operation.id !== op.id) {
          newOperationsList.push(op);
        }
      });

      this.storage.set(StorageKey.ASSETS + currency.id, newOperationsList).then(result => {
        this.removeNotification(operation);
      });
      return newOperationsList;
    });
  }

  public clear(currency: Currency) {
    return this.storage.remove(StorageKey.ASSETS + currency.id);
  }

  public removeNotification(operation: OperationData) {
    const changeData = new DepositChange(operation.type, operation.total, 0);
    this.events.publish(DepositEvent.SELL_ASSET, changeData);
    this.events.publish(CurrencyEvent.REMOVE_OPERATION, operation);
  }
}
