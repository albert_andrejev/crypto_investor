import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';

import { StorageKey } from '../../common/storage-keys';
import { Currency } from '../../common/currency';

import { CoinMarketCapProvider } from '../coin-market-cap/coin-market-cap';

import { AssetsOperationsProvider } from '../assets-operations/assets-operations';

@Injectable()
export class CurrencyOperationsProvider {

  constructor(
    private storage: Storage,
    private events: Events,
    private cmcProvider: CoinMarketCapProvider,
    private assetOps: AssetsOperationsProvider
  ) { }

  public load(simple = false) {
    return this.storage.get(StorageKey.CURRENCIES).then((currencies: Currency[]) => {
      if (currencies === null) return [];

      if (simple) return currencies;

      let currencyLoadingPromise = [];
      currencies.forEach((currencyJSON) => {
        const currency = new Currency(this.events, this.cmcProvider, this.assetOps).fromJSON(currencyJSON);
        currency.subscribeToEvents();
        
        
        const onlinePricePromise = currency.updateOnlinePrices();
        const assetPricePromise = currency.setAssetPrice(); 

        //Should be loaded with assets because of calculation issues if it will be loaded async
        const commonPromise = Promise.all([onlinePricePromise, assetPricePromise]).then(mixedData => {
          return mixedData[1];
        });

        currencyLoadingPromise.push(commonPromise);        
      });

      return Promise.all(currencyLoadingPromise).then((currencies: Currency[]) => {
        return currencies;
      });
    });
  }


  public backup() {
    return this.storage.get(StorageKey.CURRENCIES).then((currencies: Currency[]) => {
      if (currencies === null) return [];

      let currencyLoadingPromise = [];
      currencies.forEach((currency) => {
        const operationsPromise = this.assetOps.backup(currency).then(operations => {
          currency.operations = operations;
          return currency;
        });

        currencyLoadingPromise.push(operationsPromise);
      });

      return Promise.all(currencyLoadingPromise).then((currencies: Currency[]) => {
        return currencies;
      });
    });
  }

  public restore(currencies: Currency[]) {
    const unsetObj = currencies.map(currency => {
      const currencyCopy = Object.assign({}, currency);
      delete currencyCopy.operations;

      return currencyCopy;
    });

    return this.storage.set(StorageKey.CURRENCIES, unsetObj).then(_ => {
      console.log('restored currencies: ' + JSON.stringify(unsetObj));
      return unsetObj; 
    });
  }

  public remove(currency) {    
    return this.storage.get(StorageKey.CURRENCIES).then((currencies: Currency[]) => {
      let newCurrenciesList = [];
      currencies.forEach((cur) => {
        if (currency.id !== cur.id) {
          newCurrenciesList.push(cur);
        } else {
          this.assetOps.load(currency).then(operations => {
            operations.forEach(operation => {
              this.assetOps.removeNotification(operation);
            })
          });
          this.assetOps.clear(currency);
        }
      });

      this.storage.set(StorageKey.CURRENCIES, newCurrenciesList);
      return newCurrenciesList;
    });
  }

  public add(currency: Currency, idx: number = null) {
    return new Promise((resolve, reject) => {
      this.storage.get(StorageKey.CURRENCIES).then((currencies: Currency[]) => {
        if (currencies == null) {
          currencies = [];
        }

        var duplicate = currencies.filter(cur => {
          return cur.name === currency.name;
        });

        if (duplicate.length > 0) {
          reject({ reason: 'duplicate' });
          return;
        }


        if (idx !== null && idx in currencies) {
          currencies[idx] = currency;
        } else {
          currencies.push(currency)
        }

        this.storage.set(StorageKey.CURRENCIES, currencies);

        resolve(currencies);
      });
    });
  }

  public clear() {
    return this.storage.remove(StorageKey.CURRENCIES);
  }
}
