import { Pipe, PipeTransform } from '@angular/core';
import { Const } from '../../common/const';

/**
 * Generated class for the SharesConverterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'sharesConverter',
})
export class SharesConverterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    const shares = Number(value);
    if (isNaN(shares) ) {
      return NaN;
    }

    return (shares / Const.CURRENCY_NORM).toFixed(4);
  }
}
