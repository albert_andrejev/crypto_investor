import { Component } from '@angular/core';
import { NavController, PopoverController, LoadingController } from 'ionic-angular';

import { OperationsPage } from '../operations/operations';

import { CurrencyOperationsProvider } from '../../providers/currency-operations/currency-operations';
import { DepositStatsProvider } from '../../providers/deposit-stats/deposit-stats';

import { SettingsSelectorPopover } from '../popover/settings-selector/settings-selector';
import { Const } from '../../common/const';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  currencies: any[];
  totalBalance: number = 0;
  currentPortfolioCost: number = 0;
  spendTotal: number = 0;
  totalDepo: number = 0;
  totalWithdraw: number = 0;
  availableDepo: number = 0;
  coinNorm: number = Const.COIN_NORM;
  pieChartLabels:string[] = [];
  pieChartData:number[] = [];
  loadChart = false;

  constructor(
    public navCtrl: NavController,
    private depoStats: DepositStatsProvider,
    private currencyOps: CurrencyOperationsProvider,
    public loadingCtrl: LoadingController,
    public popoverCtrl: PopoverController
  ) {
    
  }

  ionViewWillEnter(): boolean {
    this.loadChart = false;
    const loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    loading.present().then(() => {
      const loagingData = [];
      const totalDepoPromise = this.depoStats.totalDepo.then(totalDep => {        
        this.totalDepo = totalDep;
        return totalDep;
      });
      loagingData.push(totalDepoPromise);

      const totalBalancePromise = this.depoStats.totalBalance.then(totalBalance => {
        this.totalBalance = totalBalance;

        return totalBalance;
      });
      loagingData.push(totalBalancePromise);

      const totalWithdrawPromise = this.depoStats.totalWithdraw.then(withdraw => {
        this.totalWithdraw = withdraw
        return withdraw;
      });
      loagingData.push(totalWithdrawPromise);

      const availableDepoPromise = this.depoStats.availableDepo.then(availableDepo => {
        this.availableDepo = availableDepo;
        return availableDepo;
      });
      loagingData.push(availableDepoPromise);

      const spendTotalPromise = this.depoStats.spendTotal.then(spendTotal => {
        this.spendTotal = spendTotal;
        return spendTotal;
      });
      loagingData.push(spendTotalPromise);

      const currencyPromise = this.currencyOps.load().then(currencies => {
        this.currencies = currencies;
        return
      });
      loagingData.push(currencyPromise);

      Promise.all(loagingData).then(data => {
        this.currentPortfolioCost = this.totalBalance - (this.availableDepo - this.totalWithdraw);
        this.pieChartLabels.length = 0;
        this.pieChartData.length = 0;
        this.currencies.forEach(currency => {
          this.pieChartLabels.push(currency.symbol);
          let part = currency.spendAssets / this.coinNorm * currency.currentPrice * 100 / this.currentPortfolioCost;
          part = Math.round(part * 100) / 100;
          this.pieChartData.push(part);
        });
        this.loadChart = this.currencies.length > 1;
        loading.dismiss();
      });
    });

    return true;
  }

  controlAssets(param) {
    if ('assetIdx' in param && param.assetIdx in this.currencies) {
      this.navCtrl.push(
        OperationsPage,
        { idx: param.assetIdx, currency: this.currencies[param.assetIdx] }
      );
    }
  }

  showSettingsPopover(myEvent) {
    let popover = this.popoverCtrl.create(SettingsSelectorPopover);
    popover.onDidDismiss(data => {
      if (data != null && 'restore' in data) {
        data.restore.then(_ => {
          this.ionViewWillEnter();
        })
          .catch(err => { });
      }
    });
    popover.present({
      ev: myEvent
    });
  }  
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
}
