import { Component } from '@angular/core';
import { ViewController, AlertController, ToastController } from 'ionic-angular';
import { File, Entry } from '@ionic-native/file';

import { InvestorData } from '../../../common/investor-data';

import { InvestorOperationsProvider } from '../../../providers/investor-operations/investor-operations';
import { DepositStatsProvider } from '../../../providers/deposit-stats/deposit-stats';
import { BackupData } from '../../../common/backup-data';
import { CurrencyOperationsProvider } from '../../../providers/currency-operations/currency-operations';
import { DepositOperationsProvider } from '../../../providers/deposit-operations/deposit-operations';
import { AssetsOperationsProvider } from '../../../providers/assets-operations/assets-operations';
import { Currency } from '../../../common/currency';

@Component({
  selector: 'page-backup-restore-modal',
  templateUrl: 'backup-restore.html'
})
export class BackupRestoreModalPage {
  backups: Entry[];

  constructor(
    private viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private depoStats: DepositStatsProvider,
    private investorOps: InvestorOperationsProvider,
    private currencyOps: CurrencyOperationsProvider,
    private depoOps: DepositOperationsProvider,
    private assetsOps: AssetsOperationsProvider,
    private file: File
  ) {
  }

  ionViewWillEnter(): boolean {    
    this.file.listDir(this.file.dataDirectory, 'backup')
      .then(files => {
        this.backups = files.sort(function (a, b) {
          if (a.name == b.name) return 0;
          return a.name > b.name ? -1 : 1;
        });
      })
      .catch(err => {
        let toast = this.toastCtrl.create({
          message: 'Backup restoration failed. Error: ' + err,
          duration: 3000
        });
        toast.present();
      });

    return true;
  }

  dismiss(data) {
    this.viewCtrl.dismiss(false);
  }

  restoreBackup(backup: Entry) {
    let prompt = this.alertCtrl.create({
      title: 'Restore backup',
      message: "Are you sure want to restore data from this backup? All not saved data will be cleared!",
      buttons: [
        {
          text: 'Cancel',
          handler: data => { }
        },
        {
          text: 'Restore',
          handler: data => {
            this.processRestoration(backup).then(_ => {
              let toast = this.toastCtrl.create({
                message: 'Backup sucessfully restored.',
                duration: 3000
              });
              toast.present();

              this.viewCtrl.dismiss(true);
            })
          }
        }
      ]
    });
    prompt.present();
  }

  processRestoration(backup: Entry) {
    console.log(backup.fullPath);
    console.log(this.file.dataDirectory + 'backup/' + backup.name);

    return this.file.readAsText(this.file.dataDirectory, 'backup/' + backup.name)
      .then(fileContent => {
        console.log(fileContent);
        const backupData: BackupData = JSON.parse(fileContent);
        return this.clearDB().then(_ => {
          console.log('start restoration');
          const waitingPromises = [];

          const currencyPromise = this.currencyOps.restore(backupData.currencies).then(_ => {
            console.log('Currencies: ' + JSON.stringify(backupData.currencies));
            backupData.currencies.forEach(currency => {
              const assetsPromise = this.assetsOps.restore(currency.operations, currency).then(_ => {
                console.log('Assets: ' + JSON.stringify(currency.operations));
                return _;
              });
              waitingPromises.push(assetsPromise);              
            });
          });
          waitingPromises.push(currencyPromise);


          const investorPromise = this.investorOps.restore(backupData.investors).then(_ => {
            console.log('Investors: ' + JSON.stringify(backupData.investors));
            backupData.investors.forEach(investor => {
              const depoPromise = this.depoOps.restore(investor.deposits, investor).then(_ => {
                console.log('Deposits: ' + JSON.stringify(investor.deposits));
                return _;
              });

              waitingPromises.push(depoPromise);
            });
            waitingPromises.push(investorPromise);
          });

          return Promise.all(waitingPromises).then(_ => {
            this.depoStats.clear();

            return true;
          });
        });
      })
      .catch(err => {
        let toast = this.toastCtrl.create({
          message: 'Backup restoration failed. Unable to open: ' + err,
          duration: 3000
        });
        toast.present();
      });
  }

  private clearDB() {    
    const currencyPromise = this.currencyOps.load(true);
    const investorPromise = this.investorOps.load(true);
    return Promise.all([currencyPromise, investorPromise]).then((backup: [Currency[], InvestorData[]]) => {
      const currencies = backup[0];
      const investors = backup[1];

      const waitingPromises = [];

      currencies.forEach(currency => {
        const assetsPromise = this.assetsOps.clear(currency).then(_ => {
          console.log('asset cleared');
          return _;
        });
        waitingPromises.push(assetsPromise);
      });

      investors.forEach(investor => {
        const depoPromise = this.depoOps.clear(investor).then(_ => {
          console.log('depos cleared');
          return _;
        });;
        waitingPromises.push(depoPromise);
      });

      return Promise.all(waitingPromises).then(_ => {
        const currencyClearPromise = this.currencyOps.clear();
        const investorClearPromise = this.investorOps.clear();

        return Promise.all([currencyClearPromise, investorClearPromise]).then(_ => {
          console.log('currencies and investors cleared');
          return true;
        });
      });

    });
  }

  removeBackup(ev, backup) {
    ev.stopPropagation();
    let prompt = this.alertCtrl.create({
      title: 'Delete backup',
      message: "Are you sure want to delete this backup? This operation can not be undone.",
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
          }
        },
        {
          text: 'Delete',
          handler: data => {
            this.file.removeFile(this.file.dataDirectory, 'backup/' + backup.name).then(_ => {
              this.ionViewWillEnter();
            });
          }
        }
      ]
    });
    prompt.present();
  }
}
