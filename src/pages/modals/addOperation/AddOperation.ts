import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { OperationData } from '../../../common/operation-data';
import { Currency } from '../../../common/currency';

import { AssetsOperationsProvider } from '../../../providers/assets-operations/assets-operations';
import { DepositStatsProvider } from '../../../providers/deposit-stats/deposit-stats';

import { FloatValidator } from '../../../validators/float.validator';
import { DepositValidator } from '../../../validators/deposit.validator';

@Component({
  selector: 'page-add-operation-modal',
  templateUrl: 'AddOperation.html'
})
export class AddOperationModalPage {  
  operation: OperationData = new OperationData();
  currency: Currency;
  availableDepo: number = 0;
  private operationForm : FormGroup;  

  constructor(
    public viewCtrl: ViewController,
    public params: NavParams,
    private formBuilder: FormBuilder,
    private depoStats: DepositStatsProvider,
    private assetsOps: AssetsOperationsProvider
  ) {
    this.currency = this.params.get('currency');

    this.operationForm = this.formBuilder.group({
      amount: ['', Validators.compose([
        Validators.required,
        Validators.min(0),
        FloatValidator.validFloat
      ])],
      total: ['', Validators.compose([
        Validators.required,
        Validators.min(0),
        FloatValidator.validFloat,
        (control: FormControl) => DepositValidator.notEnough(control, this.getAvailableDepo.bind(this))
      ])],
      date: ['', Validators.required],
    });
  }

  public getAvailableDepo(): number {
    return this.availableDepo;
  }

  ionViewWillEnter(): boolean {
    this.depoStats.availableDepo.then(availableDepo => {
      this.availableDepo = availableDepo;
    });
    
    return true;
  }

  public validation_messages = {
    'amount': [
      {type: 'required', message: 'Field is required.'},
      {type: 'validFloat', message: 'Wrong value. Should be number.'}
    ],
    'total': [
      {type: 'required', message: 'Field is required.'},
      {type: 'validFloat', message: 'Wrong value. Should be number.'},
      {type: 'notEnoughDepo', message: 'Not enough deposit.'}
    ],
    'date': [
      {type: 'required', message: 'Field is required.'}
    ]
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  addOperation() {
    this.assetsOps.buy(this.operationForm.value, this.currency).then((operations) => {
      this.viewCtrl.dismiss();  
    });
  }
}
