import { DepositType } from './deposit-type';
import CommonData from './common-data';

export class OperationData extends CommonData {
  currency_id: string;
  amount: number;
  type: DepositType;
  total: number;
  price: number;
  priceChange: number;
  date: Date;
}