import { FormControl } from '@angular/forms';
import { Const } from '../common/const';
export class DepositValidator {
  static notEnough(fc: FormControl, availableDepo: () => number){
    if ( fc.dirty === false && fc.touched === false ) return null;

    if ( fc.value === '') {
      return null;  
    }

    let floatValue = Number(fc.value);
    if (isNaN(floatValue)) {
      return null;
    }

    if (floatValue <= availableDepo() / Const.CURRENCY_NORM) {
      return null;
    } else {
      return ({notEnoughDepo: true});
    }
  }

  static notEnoughCoins(fc: FormControl, availableAssets: () => number){
    if ( fc.dirty === false && fc.touched === false ) return null;

    if ( fc.value === '') {
      return null;  
    }

    let floatValue = Number(fc.value);
    if (isNaN(floatValue)) {
      return null;
    }

    if (floatValue <= availableAssets() / Const.COIN_NORM) {
      return null;
    } else {
      return ({notEnoughCoins: true});
    }
  }

  static notEnoughShares(fc: FormControl, availableShares: () => number){
    if ( fc.dirty === false && fc.touched === false ) return null;

    if ( fc.value === '') {
      return null;  
    }

    let floatValue = Number(fc.value);
    if (isNaN(floatValue)) {
      return null;
    }

    if (floatValue <= availableShares() / Const.CURRENCY_NORM) {
      return null;
    } else {
      return ({notEnoughShares: true});
    }
  }
}