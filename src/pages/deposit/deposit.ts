import { Component } from '@angular/core';
import { ModalController, NavParams, AlertController, NavController, LoadingController } from 'ionic-angular';

import { InvestorData } from '../../common/investor-data';
import { DepositData } from '../../common/deposit-data';

import { AddDepositModalPage } from '../modals/add-deposit/add-deposit';
import { AddInvestorModalPage } from '../modals/add-investor/add-investor';
import { WithdrawModalPage } from '../modals/withdraw/withdraw';
import { isUndefined } from 'ionic-angular/util/util';

import { DepositOperationsProvider } from '../../providers/deposit-operations/deposit-operations';
import { DepositStatsProvider } from '../../providers/deposit-stats/deposit-stats';

@Component({
  selector: 'page-deposit',
  templateUrl: 'deposit.html'
})
export class DepositPage {
  deposits:DepositData[];
  investor: InvestorData;
  totalBalance: number = 0;
  totalShares: number = 0;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public params: NavParams,
    public loadingCtrl: LoadingController,
    private depoOps: DepositOperationsProvider,
    private depoStats: DepositStatsProvider
  ) { 
    this.investor = this.params.get('investor');
  }

  ionViewWillEnter(): boolean {    
    this.loadDeposits();
    return true;
  }  

  private loadDeposits() {
    this.depoStats.totalBalance.then(balance => {
      this.totalBalance = balance;
    });
    this.depoStats.totalShares.then(shares => {
      this.totalShares = shares;  
    });

    this.depoOps.load(this.investor).then((deposits) => {
      this.deposits = deposits;      
    });  
  }

  openDepositModal(params: any = {}) {
    params.investor = this.investor;
    let modal = this.modalCtrl.create(AddDepositModalPage, params);
    modal.onDidDismiss((deposit) => {
      this.investor.deposit += deposit;
      this.loadDeposits();
    });
    modal.present();
  }

  openWithdrawModal(params: any = {}) {
    params.investor = this.investor;
    let modal = this.modalCtrl.create(WithdrawModalPage, params);
    modal.onDidDismiss((withdrawDepo) => {
      this.investor.deposit -= withdrawDepo;
      this.loadDeposits();
    });
    modal.present();
  }

  removeDeposit(ev, deposit: DepositData) {
    ev.stopPropagation();
    let prompt = this.alertCtrl.create({
      title: 'Delete deposit',
      message: "Are you sure want to delete this deposit operation?",
      buttons: [
        {
          text: 'Cancel',
          handler: data => {}
        },
        {
          text: 'Delete',
          handler: data => {
            this.depoOps.remove(deposit, this.investor).then((deposits) => {
              this.deposits = deposits;  
              this.depoStats.totalBalance.then(balance => {
                this.totalBalance = balance;
              });

              this.totalShares -= deposit.shares;  
              this.investor.shares -= deposit.shares;
            });
          }
        }
      ]
    });
    prompt.present();
  }  

  openInvestorModal() {
    let modal = this.modalCtrl.create(AddInvestorModalPage, {investor: this.investor});
    modal.onDidDismiss((investor: InvestorData)=> {
      if (investor !== null && !isUndefined(investor)) {
        this.investor = investor; 
      }      
    });
    modal.present();
  }
}
