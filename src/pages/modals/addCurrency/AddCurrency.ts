import { Component } from '@angular/core';
import { ViewController, NavParams, LoadingController, ToastController } from 'ionic-angular';

import { CoinMarketCapProvider } from '../../../providers/coin-market-cap/coin-market-cap';
import { CurrencyOperationsProvider } from '../../../providers/currency-operations/currency-operations';

import { Currency } from '../../../common';

@Component({
  selector: 'page-add-config-modal',
  templateUrl: 'AddCurrency.html'
})
export class AddCurrencyModalPage {
  currencies: Currency[];
  private currenciesList: Currency[];
  isNew: boolean;
  private curIdx: number;

  constructor(
    public viewCtrl: ViewController,
    public params: NavParams,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private currencyOps: CurrencyOperationsProvider,
    cmcProvider: CoinMarketCapProvider
  ) {
    console.log("modal loaded");
    this.isNew = true;
    this.curIdx = this.params.get('curIdx');

    if (this.curIdx != null) {
      this.isNew = false;
    }

    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });

    loading.present();

    cmcProvider.getCurrenciesList().then((currencies: Currency[]) => {
      this.currencies = currencies;
      this.currenciesList = currencies;
      loading.dismiss();
    });
  }

dismiss() {
  this.viewCtrl.dismiss();
}

getCurrencies(ev: any) {
  // set val to the value of the searchbar
  let val = ev.target.value.toLowerCase();

  // if the value is an empty string don't filter the items
  if (val && val.trim() != '') {
    this.currencies = this.currenciesList.filter((item: Currency) => {
      if (item.symbol.toLowerCase().indexOf(val) > -1
        || item.name.toLowerCase().indexOf(val) > -1) {
        return true;
      }

      return false;
    });
  }
}
addCurrency(currency: Currency) {
  this.currencyOps.add(currency, this.curIdx).then(currencies => {
    this.viewCtrl.dismiss();
  })
    .catch(error => {
      if (error.reason === 'duplicate') {
        let toast = this.toastCtrl.create({
          message: 'Currency already added',
          duration: 3000
        });
        toast.present();
      }
    });
}
}
