import { DepositData } from './deposit-data';
import { DepositType } from './deposit-type';
import CommonData from './common-data';

import {DepositOperationsProvider} from '../providers/deposit-operations/deposit-operations';

export class InvestorData extends CommonData {
  isMain: boolean;
  firstName: string;
  lastName: string;
  name: string;
  email: string;
  phone: string;
  description: string;
  deposit: number = 0;
  withdraw: number = 0;
  shares: number = 0;
  deposits: DepositData[];

  calcFields(depoOps: DepositOperationsProvider) {
    this.deposit = 0;
    this.shares = 0;
    this.withdraw = 0;
    return depoOps.load(this).then((deposits: DepositData[]) => {     
      deposits.forEach(deposit => {
        let depo = Number(deposit.deposit);
        let share = Number(deposit.shares); 
        if (!isNaN(depo) && !isNaN(share)) {
          if (deposit.type == DepositType.DEPOSIT) {
            this.deposit += depo;            
          } else if (deposit.type == DepositType.WITHDRAW) {
            this.withdraw += depo;
          }           
          this.shares += share;
        }
      });
              
      return (this.deposit, this.shares, this.withdraw);
    });
  }

  get fullName():string {
    if (this.name) {
      return this.name;
    }

    return this.firstName + " " + this.lastName;
  }
}