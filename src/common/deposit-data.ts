import CommonData from './common-data';
import { DepositType } from './deposit-type';



export class DepositChange  {
    private _type: DepositType
    get type(): DepositType {
        return this._type;
    }

    private _deposit: number;
    get deposit(): number {
        return this._deposit;
    }

    private _shares: number;
    get shares(): number {
        return this._shares;
    }

    constructor(
        type: DepositType, 
        deposit: number, 
        shares: number) 
    {
        this._type = type;
        this._deposit = Number(deposit);
        this._shares = Number(shares);
    }    
}

export class DepositData extends CommonData {
    investorFK: number;
    type: DepositType;
    deposit: number;
    shares: number;
    currency: string;
    date: Date;
    comment: string;    
}