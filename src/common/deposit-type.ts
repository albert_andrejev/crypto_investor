export enum DepositType {
    DEPOSIT = "deposit",
    WITHDRAW = "withdraw",
    BUY_ASSET = "buy_asset",
    SELL_ASSET = "sell_asset"
}