import { SystemEvent } from './system-event';

export class CurrencyEvent {
    public static EVENT_TYPE: string = SystemEvent.CURRENCY;
    public static REMOVE_OPERATION: string = CurrencyEvent.EVENT_TYPE + 'remove_operation';
}